/**
 * @author Lucas Wagner 
 */

package br.com.ozcorp;

public enum Sexo {
	
	MASCULINO("Masculino"),
	FEMININO("Feminino"),
	OUTRO("Outro");
	
	public String nome;
	
	Sexo(String nome){
		this.nome = nome;
	}
}
