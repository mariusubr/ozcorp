/**
 * @author Lucas Wagner 
 */

package br.com.ozcorp;

public class Cargo {

	String titulo;
	double salarioBase;

	public Cargo(String titulo, double salarioBase) {
		this.titulo = titulo;
		this.salarioBase = salarioBase;	
	}

	// Getters & Setters
	
	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {		
		this.titulo = titulo;
	}

	public double getSalarioBase() {
		return salarioBase;
	}

	public void setSalarioBase(double salarioBase) {
		if(salarioBase > 0) {
		this.salarioBase = salarioBase;
		}else {
			System.err.println("Sal�rio inv�lido!");
		}
	}
		
}
