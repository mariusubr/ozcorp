/**
 * @author Lucas Wagner 
 */

package br.com.ozcorp;

public enum TipoSanguineo {
	
	A_POSITIVO ("A_Positivo"),
	A_NEGATIVO ("A_Negativo"),
	AB_POSITIVO("AB_Positivo"),
	B_POSITIVO ("B_Positivo"),
	B_NEGATIVO ("B_Negativo"),
	AB_NEGATIVO("AB_Negativo"),
	O_POSITIVO ("O_Positivo"),
	O_NEGATIVO ("O_Negativo");
	
	public String tiposanguineo;
	
	TipoSanguineo(String tiposanguineo){
		this.tiposanguineo = tiposanguineo;
	}
}

