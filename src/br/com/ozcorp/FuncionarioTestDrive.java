/**
 * @author Lucas Wagner 
 */

package br.com.ozcorp;

public class FuncionarioTestDrive {

	public static void main(String[] args) {

		Cargo diretorFinanceiro = new Cargo("Diretor financeiro", 14000.00);
		Cargo analistaFinanceiro = new Cargo("Analista financeiro", 8750.00);
		Cargo contadorFinanceiro = new Cargo("Contador", 2500.00);
		Cargo engenheiroFinanceiro = new Cargo("Engenheiro", 30000.00);
		Cargo secretarioFinanceiro = new Cargo("Secretario", 750.00);

		Departamento diretores = new Departamento("Diretores financeiros", "D.F.", diretorFinanceiro);
		Departamento analistas = new Departamento("Analistas", "A.F.", analistaFinanceiro);
		Departamento contadores = new Departamento("Contadores", "Cnts", contadorFinanceiro);
		Departamento engenheiro = new Departamento("Engenheiro", "Eng", engenheiroFinanceiro);
		Departamento secretario = new Departamento("Secretario", "Secre", secretarioFinanceiro);

		Funcionario wagner = new Funcionario("Wagner", "500747009", "47649712899", "4969", "wagner@gmail.com", "senha",
				Sangue.O_NEG, Sexo.MASCULINO, 1, engenheiro);
		Funcionario bia = new Funcionario("Bia", "827364828", "12492015821", "5643", "bia@gmail.com", "senha",
				Sangue.A_NEG, Sexo.FEMININO, 1, secretario);
		Funcionario diogo = new Funcionario("Diogo", "564231951", "156498543129", "9982", "diogo@gmail.com", "senha",
				Sangue.O_POS, Sexo.MASCULINO, 1, diretores);
		Funcionario fabiola = new Funcionario("Fabiola", "658959817", "0215515814", "1256", "fabiola@gmail.com", "senha",
				Sangue.B_NEG, Sexo.FEMININO, 1, analistas);
		Funcionario carlos = new Funcionario("Carlos", "748518483", "535485413", "0090", "carlos@gmail.com", "senha",
				Sangue.AB_NEG, Sexo.MASCULINO, 1, contadores);

		System.out.println("DADOS DO FUNCIONARIO: 1");
		System.out.println("");
		System.out.println("Nome            : " + wagner.getNome());
		System.out.println("RG              : " + wagner.getRg());
		System.out.println("CPF             : " + wagner.getCpf());
		System.out.println("Matrícula       : " + wagner.getMatricula());
		System.out.println("E-Mail          : " + wagner.getEmail());
		System.out.println("Senha           : " + wagner.getSenha());
		System.out.println("Tipo sanguíneo  : " + wagner.getSangue());
		System.out.println("Sexo            : " + wagner.getSexo());
		System.out.println("DADOS DO DEPARTAMENTO: ");
		System.out.println("");
		System.out.println("Cargo           : " + wagner.getDepartamento().getCargo().getTitulo());
		System.out.println("Salário base    : " + wagner.getDepartamento().getCargo().getSalarioBase());

		System.out.println("_________________________________________________________________________________");

		System.out.println("DADOS DO FUNCIONARIO: 2");
		System.out.println("");
		System.out.println("Nome            : " + bia.getNome());
		System.out.println("RG              : " + bia.getRg());
		System.out.println("CPF             : " + bia.getCpf());
		System.out.println("Matrícula       : " + bia.getMatricula());
		System.out.println("E-Mail          : " + bia.getEmail());
		System.out.println("Senha           : " + bia.getSenha());
		System.out.println("Tipo sanguíneo  : " + bia.getSangue());
		System.out.println("Sexo            : " + bia.getSexo());
		System.out.println("DADOS DO DEPARTAMENTO: ");
		System.out.println("");
		System.out.println("Cargo           : " + bia.getDepartamento().getCargo().getTitulo());
		System.out.println("Salário base    : " + bia.getDepartamento().getCargo().getSalarioBase());

		System.out.println("_________________________________________________________________________________");

		System.out.println("DADOS DO FUNCIONARIO: 3");
		System.out.println("");
		System.out.println("Nome            : " + diogo.getNome());
		System.out.println("RG              : " + diogo.getRg());
		System.out.println("CPF             : " + diogo.getCpf());
		System.out.println("Matrícula       : " + diogo.getMatricula());
		System.out.println("E-Mail          : " + diogo.getEmail());
		System.out.println("Senha           : " + diogo.getSenha());
		System.out.println("Tipo sanguíneo  : " + diogo.getSangue());
		System.out.println("Sexo            : " + diogo.getSexo());
		System.out.println("DADOS DO DEPARTAMENTO: ");
		System.out.println("");
		System.out.println("Cargo           : " + diogo.getDepartamento().getCargo().getTitulo());
		System.out.println("Salário base    : " + diogo.getDepartamento().getCargo().getSalarioBase());

		System.out.println("_________________________________________________________________________________");

		System.out.println("DADOS DO FUNCIONARIO: 4");
		System.out.println("");
		System.out.println("Nome            : " + fabiola.getNome());
		System.out.println("RG              : " + fabiola.getRg());
		System.out.println("CPF             : " + fabiola.getCpf());
		System.out.println("Matrícula       : " + fabiola.getMatricula());
		System.out.println("E-Mail          : " + fabiola.getEmail());
		System.out.println("Senha           : " + fabiola.getSenha());
		System.out.println("Tipo sanguíneo  : " + fabiola.getSangue());
		System.out.println("Sexo            : " + fabiola.getSexo());
		System.out.println("DADOS DO DEPARTAMENTO: ");
		System.out.println("");
		System.out.println("Cargo           : " + fabiola.getDepartamento().getCargo().getTitulo());
		System.out.println("Salário base    : " + fabiola.getDepartamento().getCargo().getSalarioBase());

		System.out.println("_________________________________________________________________________________");

		System.out.println("DADOS DO FUNCIONARIO: 5");
		System.out.println("");
		System.out.println("Nome            : " + carlos.getNome());
		System.out.println("RG              : " + carlos.getRg());
		System.out.println("CPF             : " + carlos.getCpf());
		System.out.println("Matrícula       : " + carlos.getMatricula());
		System.out.println("E-Mail          : " + carlos.getEmail());
		System.out.println("Senha           : " + carlos.getSenha());
		System.out.println("Tipo sanguíneo  : " + carlos.getSangue());
		System.out.println("Sexo            : " + carlos.getSexo());
		System.out.println("DADOS DO DEPARTAMENTO: ");
		System.out.println("");
		System.out.println("Cargo           : " + carlos.getDepartamento().getCargo().getTitulo());
		System.out.println("Salário base    : " + carlos.getDepartamento().getCargo().getSalarioBase());
	}
}
